//paletas de colores (Array)
//colores en codigo hexadeciaml
const paletaColores1 = ['#33A7FF', '#1C4869', '#688AA4'];
const paletaColores2 = ['#CEDA54', '#676F17', '#9A9F6B'];
const paletaColores3 = ['#7A9F6B', '#316E1A', '#B6D5AA'];

//esta funcion se usa como constructor
function brick(altura, ancho, paletaColores, jump) {

  //vars
  this.altura = altura;
  this.ancho = ancho;
  this.paletaColores = paletaColores;
  this.panelContainer = null;
  this.refBrick = null;
  this.jump = jump;

  //creamos el template de nuestro ladrillo
  this.brickTpl = '<div class="brick" style="width:' + this.ancho + 'px; height:' + this.altura + 'px;"></div>';

  //methods
  this.init = function () {
    //trick para guarda el contexto actual y utilizarlo en donde no exista
    const self = this;

    //llamamos la funciona para renderizar el brick en el DOM
    this.render();

    //esta funcion se encarga de inyectar los eventos en el document
    this.jump > 0 ? this.addEvents(self) : null;

    //con setInterval vamos a cambiar el color del brick cada x cantidad de segundos
    //donde x es el 2dn parametro de la funcion
    setInterval(function () {
      self.changeColor();
    },
      350);
  }

  this.render = function () {
    //seleccionamos el elemento en el que vamos a insertar nuestro ladrillo
    panelContainer = document.querySelector('.panel-container');

    //insertamos el ladrillo
    panelContainer.insertAdjacentHTML('afterbegin', this.brickTpl);

    //guardamos la referencial del ladriollo
    this.refBrick = document.querySelector('.brick');
  }

  //se encarga de cambiar el color
  this.changeColor = function () {
    //seleccionamos un numero al azar entre 0 y 2
    //sacamos ese index del arreglo (paleta de colores)
    const randomColor = this.paletaColores[Math.round(Math.random() * 2)];

    //setteamos el color a brick
    this.refBrick.style.background = randomColor;
  }

  //agrega los eventos al DOM
  this.addEvents = function (self) {
    //este evento escucha por las teclas presionadas
    document.addEventListener('keydown', function (event) {

      //aqui movemos el brick hacia arriba
      if (event.which === 38) {
        //si la posicion es igual a '' le asignamos un 0
        self.refBrick.style.top === '' ? self.refBrick.style.top = 0 : null;

        //aqui movemos el brick cada 25px
        self.refBrick.style.top = (parseInt(self.refBrick.style.top.replace('px', '')) - self.jump) + 'px';
      }

      //aqui movemos el brick hacia abajo
      if (event.which === 40) {
        //si la posicion es igual a '' le asignamos un 0
        self.refBrick.style.top === '' ? self.refBrick.style.top = 0 : null;

        //aqui movemos el brick cada 25px
        self.refBrick.style.top = (parseInt(self.refBrick.style.top.replace('px', '')) + self.jump) + 'px';
      }

      //aqui movemos el brick hacia izquierda
      if (event.which === 39) {
        //si la posicion es igual a '' le asignamos un 0
        self.refBrick.style.left === '' ? self.refBrick.style.left = 0 : null;

        //aqui movemos el brick cada 25px
        self.refBrick.style.left = (parseInt(self.refBrick.style.left.replace('px', '')) + self.jump) + 'px';
      }

      //aqui movemos el brick hacia derecha
      if (event.which === 37) {
        //si la posicion es igual a '' le asignamos un 0
        self.refBrick.style.left === '' ? self.refBrick.style.left = 0 : null;

        //aqui movemos el brick cada 25px
        self.refBrick.style.left = (parseInt(self.refBrick.style.left.replace('px', '')) - self.jump) + 'px';
      }
    });
  }
}

//creamos la instancia de un ladrillo
const brick1 = new brick(50, 60, paletaColores1, 35);
const brick2 = new brick(250, 300, paletaColores3);
const brick3 = new brick(500, 300, paletaColores2, 25);

//ini del module
brick1.init();
brick2.init();
brick3.init();